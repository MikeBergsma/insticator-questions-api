## Questions API - Insticator Coding Test

### Technologies
- JDK 1.8
- Maven
- Spring Boot
- PostgreSQL
- Hibernate

### Steps to Setup

**1. Clone the repository**

```bash
git clone git@gitlab.com:MikeBergsma/insticator-questions-api.git
```

**2. Configure PostgreSQL**

First, create a database named `insticator_questions`. Then, open `src/main/resources/application.properties` file and change the spring datasource username and password as per your PostgreSQL installation.

**3. Run the app**

Type the following command from the root directory of the project to run it -

```bash
mvnw spring-boot:run
```

**4. Review the docs**

The API documentation is located in `src/main/resources/static/index.html` and can be accessed via the route below -

```bash
https://localhost:8080/docs
```

It can be regenerated with the following command -

```bash
mvnw generate-resources
```

**5. Utilize Postman**

The Postman collection located in the root of the project (`/Questions-API.postman_collection.json`) contains examples of every request in the API and is useful for populating the database with some initial questions.
