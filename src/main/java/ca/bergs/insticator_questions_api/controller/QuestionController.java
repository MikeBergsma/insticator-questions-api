package ca.bergs.insticator_questions_api.controller;

import ca.bergs.insticator_questions_api.exception.BadRequestException;
import ca.bergs.insticator_questions_api.exception.ResourceNotFoundException;
import ca.bergs.insticator_questions_api.model.*;
import ca.bergs.insticator_questions_api.pojo.BaseQuestion;
import ca.bergs.insticator_questions_api.view.ViewType;
import ca.bergs.insticator_questions_api.repository.AnswerRepository;
import ca.bergs.insticator_questions_api.repository.QuestionRepository;
import ca.bergs.insticator_questions_api.service.QuestionFactory;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.net.URI;
import java.util.Optional;
import java.util.UUID;

@RestController
public class QuestionController {

    private QuestionRepository questionRepository;
    private AnswerRepository answerRepository;

    public QuestionController(QuestionRepository questionRepository, AnswerRepository answerRepository) {
       this.questionRepository = questionRepository;
        this.answerRepository = answerRepository;
    }

    // TODO: Implement HATEOAS-based responses
    // TODO: Implement API versioning

    @PostMapping("/questions")
    @JsonView(ViewType.Internal.class)
    public ResponseEntity<BaseQuestion> create(@Valid @RequestBody Question questionModel) {
        // TODO: Implement validation for option properties (eg. AnswerData.AnswerOptions[])
        try {
            BaseQuestion question = QuestionFactory.createFromModel(questionModel);
            question.initializeFromModel(questionRepository.save(questionModel));
            return ResponseEntity.created(URI.create("/questions/" + question.getId())).body(question);
        }
        catch (RuntimeException err) {
            throw new BadRequestException(err.getMessage());
        }
    }

    @GetMapping("/questions/{questionId}")
    @JsonView(ViewType.Public.class)
    public BaseQuestion findById(@PathVariable UUID questionId) {
        return questionRepository.findById(questionId)
                .map(QuestionFactory::createFromModel)
                .orElseThrow(() -> new ResourceNotFoundException("Question not found with id " + questionId));
    }

    @GetMapping("/questions/random")
    @JsonView(ViewType.Public.class)
    public BaseQuestion findRandomOneByUserId(@RequestParam("userId") UUID userId,
                                              @RequestParam(value = "tags", required = false) String tags) {
        Optional<Question> randomQuestion = (tags != null && tags.length() > 0) ?
                questionRepository.findOneRandomForUserByTags(userId, tags) :
                questionRepository.findOneRandomForUser(userId);

        return randomQuestion
                .map(QuestionFactory::createFromModel)
                .orElseThrow(() -> new ResourceNotFoundException("No available questions for user"));
    }

    @GetMapping("/questions")
    @JsonView(ViewType.Internal.class)
    public Page<BaseQuestion> findAll(@RequestParam(value = "tags", required = false) String tags,
                                      Pageable pageable) {
        return (tags != null && tags.length() > 0) ?
                questionRepository.findAllByTags(tags, pageable).map(QuestionFactory::createFromModel) :
                questionRepository.findAll(pageable).map(QuestionFactory::createFromModel);
    }

    @PutMapping("/questions/{questionId}")
    @JsonView(ViewType.Internal.class)
    public BaseQuestion update(@PathVariable UUID questionId, @Valid @RequestBody Question requestQuestion) {
        return questionRepository.findById(questionId)
                .map(questionModel -> {
                    if (answerRepository.findByQuestionId(questionModel.getId(), Pageable.unpaged()).getTotalElements() > 0) {
                        throw new BadRequestException("Questions can not be updated once they have answers.");
                    }

                    try {
                        BaseQuestion question = QuestionFactory.createFromModel(requestQuestion);

                        questionModel.setTitle(requestQuestion.getTitle());
                        questionModel.setDescription(requestQuestion.getDescription());
                        questionModel.setFormat(requestQuestion.getFormat());
                        questionModel.setType(requestQuestion.getType());
                        questionModel.setTags(requestQuestion.getTags());
                        questionModel.setAnswers(requestQuestion.getAnswers());

                        question.initializeFromModel(questionRepository.save(questionModel));

                        return question;
                    }
                    catch (Error err) {
                        throw new BadRequestException(err.getMessage());
                    }
                })
                .orElseThrow(() -> new ResourceNotFoundException("Question not found with id " + questionId));
    }

    @DeleteMapping("/questions/{questionId}")
    @JsonView(ViewType.Internal.class)
    public ResponseEntity destroy(@PathVariable UUID questionId) {
        return questionRepository.findById(questionId)
                .map(question -> {
                    questionRepository.delete(question);
                    return ResponseEntity.noContent().build();
                })
                .orElseThrow(() -> new ResourceNotFoundException("Question not found with id " + questionId));
    }
}
