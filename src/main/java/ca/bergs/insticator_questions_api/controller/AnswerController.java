package ca.bergs.insticator_questions_api.controller;

import ca.bergs.insticator_questions_api.exception.BadRequestException;
import ca.bergs.insticator_questions_api.exception.ResourceNotFoundException;
import ca.bergs.insticator_questions_api.model.Answer;
import ca.bergs.insticator_questions_api.model.Question;
import ca.bergs.insticator_questions_api.pojo.BaseQuestion;
import ca.bergs.insticator_questions_api.view.AnswerView;
import ca.bergs.insticator_questions_api.view.ViewType;
import ca.bergs.insticator_questions_api.repository.AnswerRepository;
import ca.bergs.insticator_questions_api.repository.QuestionRepository;
import ca.bergs.insticator_questions_api.view.AnswerViewFactory;
import ca.bergs.insticator_questions_api.service.QuestionFactory;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.UUID;

@RestController
public class AnswerController {

    private AnswerRepository answerRepository;
    private QuestionRepository questionRepository;

    public AnswerController(AnswerRepository answerRepository, QuestionRepository questionRepository) {
        this.answerRepository = answerRepository;
        this.questionRepository = questionRepository;
    }

    @PostMapping("/questions/{questionId}/answers")
    @JsonView(ViewType.WithQuestion.class)
    public ResponseEntity<AnswerView> create(@PathVariable UUID questionId, @Valid @RequestBody Answer answer) {
        Question questionModel = questionRepository.findById(questionId)
                .orElseThrow(() -> new ResourceNotFoundException("Question not found with id " + questionId));

        try {
            QuestionFactory.createFromModel(questionModel).checkAnswer(answer);
        }
        catch (RuntimeException err) {
            throw new BadRequestException(err.getMessage());
        }

        answer.setQuestion(questionModel);
        Answer newAnswer = answerRepository.save(answer);
        AnswerView answerView = AnswerViewFactory.createViewFromModel(newAnswer);

        String uri = String.format("/questions/%s/answers/%s", questionId, newAnswer.getId());
        return ResponseEntity.created(URI.create(uri)).body(answerView);
    }

    @GetMapping("/questions/{questionId}/answers/{answerId}")
    @JsonView(ViewType.WithQuestion.class)
    public AnswerView findById(@PathVariable UUID questionId, @PathVariable UUID answerId) {
        if (!questionRepository.existsById(questionId)) {
            throw new ResourceNotFoundException("Question not found with id " + questionId);
        }

        return answerRepository.findById(answerId)
                .map(AnswerViewFactory::createViewFromModel)
                .orElseThrow(() -> new ResourceNotFoundException("Answer not found with id " + answerId));
    }

    @GetMapping("/questions/{questionId}/answers")
    @JsonView(ViewType.WithoutQuestion.class)
    public Page<AnswerView> findAllByQuestionId(@PathVariable UUID questionId,
                                                @RequestParam(value = "userId", required = false) UUID userId,
                                                Pageable pageable) {
        if (!questionRepository.existsById(questionId)) {
            throw new ResourceNotFoundException("Question not found with id " + questionId);
        }

        return (userId != null) ?
                answerRepository.findByQuestionIdAndUserId(questionId, userId, pageable).map(AnswerViewFactory::createViewFromModel) :
                answerRepository.findByQuestionId(questionId, pageable).map(AnswerViewFactory::createViewFromModel);
    }

    @GetMapping("/answers")
    @JsonView(ViewType.WithQuestion.class)
    public Page<AnswerView> findAllByUserId(@RequestParam("userId") UUID userId, Pageable pageable) {
        return answerRepository.findByUserId(userId, pageable).map(AnswerViewFactory::createViewFromModel);
    }

    @PutMapping("/questions/{questionId}/answers/{answerId}")
    @JsonView(ViewType.WithoutQuestion.class)
    public AnswerView update(@PathVariable UUID questionId,
                             @PathVariable UUID answerId,
                             @Valid @RequestBody Answer answerRequest) {
        // TODO: Clarify use cases for updating an answer

        Question questionModel = questionRepository.findById(questionId)
                .orElseThrow(() -> new ResourceNotFoundException("Question not found with id " + questionId));

        return answerRepository.findById(answerId)
                .map(answer -> {
                    answer.setValues(answerRequest.getValues());

                    try {
                        QuestionFactory.createFromModel(questionModel).checkAnswer(answer);
                    }
                    catch (RuntimeException err) {
                        throw new BadRequestException(err.getMessage());
                    }

                    return AnswerViewFactory.createViewFromModel(answerRepository.save(answer));
                })
                .orElseThrow(() -> new ResourceNotFoundException("Answer not found with id " + answerId));
    }

    @DeleteMapping("/questions/{questionId}/answers/{answerId}")
    public ResponseEntity destroy(@PathVariable UUID questionId, @PathVariable UUID answerId) {
        if (!questionRepository.existsById(questionId)) {
            throw new ResourceNotFoundException("Question not found with id " + questionId);
        }

        return answerRepository.findById(answerId)
                .map(answer -> {
                    answerRepository.delete(answer);
                    return ResponseEntity.noContent().build();
                })
                .orElseThrow(() -> new ResourceNotFoundException("Answer not found with id " + answerId));
    }

    @DeleteMapping("/answers")
    public ResponseEntity destroyAllByUserId(@RequestParam("userId") UUID userId) {
        answerRepository.deleteAllByUserId(userId);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/questions/{questionId}/answers")
    public ResponseEntity destroyAllByQuestionId(@PathVariable UUID questionId,
                                                 @RequestParam(value = "userId", required = false) UUID userId) {
        if (!questionRepository.existsById(questionId)) {
            throw new ResourceNotFoundException("Question not found with id " + questionId);
        }

        if (userId != null) {
            answerRepository.deleteAllByQuestionIdAndUserId(questionId, userId);
        } else {
            answerRepository.deleteAllByQuestionId(questionId);
        }

        return ResponseEntity.noContent().build();
    }
}
