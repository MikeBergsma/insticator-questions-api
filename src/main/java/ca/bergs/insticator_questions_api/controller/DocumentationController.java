package ca.bergs.insticator_questions_api.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DocumentationController {
    @RequestMapping("/docs")
    public String index() {
        return "index.html";
    }
}
