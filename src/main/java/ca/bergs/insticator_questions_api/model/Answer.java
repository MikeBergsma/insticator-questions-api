package ca.bergs.insticator_questions_api.model;

import ca.bergs.insticator_questions_api.view.ViewType;
import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Table(name = "answers")
public class Answer extends AuditModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonView(ViewType.WithoutQuestion.class)
    private UUID id;
    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    @NotNull
    @JsonView(ViewType.WithoutQuestion.class)
    private UUID userId;
    public UUID getUserId() {
        return userId;
    }
    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "question_id", nullable = false, foreignKey = @ForeignKey(name = "answers_question_id_fkey"))
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonView(ViewType.WithQuestion.class)
    private Question question;
    public Question getQuestion() { return question; }
    public void setQuestion(Question question) { this.question = question; }

    @NotNull
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonView(ViewType.WithoutQuestion.class)
    private String[] values;
    public String[] getValues() {
        return values;
    }
    public void setValues(String[] values) {
        this.values = values;
    }

    @JsonView(ViewType.WithoutQuestion.class)
    private Boolean isCorrect;
    public Boolean getIsCorrect() { return this.isCorrect; }
    public void setIsCorrect(Boolean isCorrect) { this.isCorrect = isCorrect; }
}
