package ca.bergs.insticator_questions_api.model;

import ca.bergs.insticator_questions_api.pojo.AnswerData;
import ca.bergs.insticator_questions_api.pojo.QuestionFormat;
import ca.bergs.insticator_questions_api.pojo.QuestionType;
import com.vladmihalcea.hibernate.type.array.StringArrayType;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

@Entity
@Table(name = "questions")
@TypeDefs({
        @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class),
        @TypeDef(name = "varchar[]", typeClass = StringArrayType.class)
})
public class Question extends AuditModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    public UUID getId() { return id; }
    public void setId(UUID id) { this.id = id; }

    @NotBlank
    @NotNull
    @Size(min = 3, max = 100)
    private String title;
    public String getTitle() { return title; }
    public void setTitle(String title) { this.title = title; }

    @Column(columnDefinition = "text")
    private String description;
    public String getDescription() { return description; }
    public void setDescription(String description) { this.description = description; }

    @Column(columnDefinition = "varchar")
    @NotNull
    private QuestionType type;
    @Enumerated(EnumType.STRING)
    public QuestionType getType() { return type; }
    public void setType(QuestionType type) { this.type = type; }

    @Column(columnDefinition = "varchar")
    @NotNull
    private QuestionFormat format;
    @Enumerated(EnumType.STRING)
    public QuestionFormat getFormat() { return format; }
    public void setFormat(QuestionFormat format) { this.format = format; }

    @Type(type = "varchar[]")
    @Column(columnDefinition = "varchar[]")
    private String[] tags;
    public String[] getTags() { return tags; }
    public void setTags(String[] tags) { this.tags = tags; }

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private AnswerData answers;
    public AnswerData getAnswers() { return answers; }
    public void setAnswers(AnswerData answers) { this.answers = answers; }
}
