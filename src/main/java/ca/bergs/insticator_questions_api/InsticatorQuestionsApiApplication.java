package ca.bergs.insticator_questions_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.web.config.EnableSpringDataWebSupport;

@SpringBootApplication(scanBasePackages={"ca.bergs"})
@EnableJpaAuditing
@EnableSpringDataWebSupport
public class InsticatorQuestionsApiApplication {
	public static void main(String[] args) {
		SpringApplication.run(InsticatorQuestionsApiApplication.class, args);
	}
}


