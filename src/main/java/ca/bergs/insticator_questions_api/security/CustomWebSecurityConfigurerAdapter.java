package ca.bergs.insticator_questions_api.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class CustomWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

    private final CustomBasicAuthenticationEntryPoint authenticationEntryPoint;

    @Autowired
    public CustomWebSecurityConfigurerAdapter(CustomBasicAuthenticationEntryPoint authenticationEntryPoint) {
        this.authenticationEntryPoint = authenticationEntryPoint;
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        // TODO: Implement better auth framework and remove hard-coded credentials
        auth.inMemoryAuthentication()
                .withUser("admin").password(passwordEncoder().encode("password"))
                .authorities("ROLE_ADMIN");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                    .antMatchers(HttpMethod.GET, "/docs").permitAll()
                    .antMatchers(HttpMethod.GET, "/questions/*").permitAll()
                    .antMatchers(HttpMethod.POST, "/questions/*/answers").permitAll()
                    .antMatchers(HttpMethod.GET, "/questions/*/answers/*").permitAll()
                    .anyRequest().authenticated()
                .and()
                .httpBasic()
                .realmName("insticator-questions-api")
                .authenticationEntryPoint(authenticationEntryPoint);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
