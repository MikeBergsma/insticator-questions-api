package ca.bergs.insticator_questions_api.pojo;

import ca.bergs.insticator_questions_api.view.ViewType;
import com.fasterxml.jackson.annotation.JsonView;

public class AnswerOption {
    public AnswerOption() {
    }

    public AnswerOption(String value, String label) {
        this.value = value;
        this.label = label;
    }

    @JsonView(ViewType.Public.class)
    private String value;
    public String getValue() { return value; }
    public void setValue(String value) { this.value = value; }

    @JsonView(ViewType.Public.class)
    private String label;
    public String getLabel() { return label; }
    public void setLabel(String label) { this.label = label; }
}
