package ca.bergs.insticator_questions_api.pojo;

import ca.bergs.insticator_questions_api.exception.InvalidAnswerException;
import ca.bergs.insticator_questions_api.exception.InvalidQuestionException;
import ca.bergs.insticator_questions_api.model.Answer;
import ca.bergs.insticator_questions_api.model.Question;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public abstract class ListQuestion extends BaseQuestion {

    @Override
    public void initializeFromModel(Question questionModel) {
        super.initializeFromModel(questionModel);

        if (this.answerData == null) {
            throw new InvalidQuestionException("Answer data is required.");
        }

        Set<String> optionValues = new HashSet<>();
        Set<String> optionLabels = new HashSet<>();
        for (AnswerOption option : this.answerData.getOptions()) {
            optionValues.add(option.getValue());
            optionLabels.add(option.getLabel());
        }

        if (optionValues.size() != this.answerData.getOptions().length) {
            throw new InvalidQuestionException("Answer options must have unique values.");
        }

        if (optionLabels.size() != this.answerData.getOptions().length) {
            throw new InvalidQuestionException("Answer options must have unique labels.");
        }

        if (this.answerData.getMinRequired() == null) {
            this.answerData.setMinRequired(1);
        }

        if (this.answerData.getMaxAllowed() == null) {
            this.answerData.setMaxAllowed(1);
        }

        if (this.answerData.getMinRequired() < 0) {
            throw new InvalidQuestionException("The minimum number of required answers can not be less than 0.");
        }

        if (this.answerData.getMaxAllowed() < 1) {
            throw new InvalidQuestionException("The maximum number of required answers allowed must be greater than 0.");
        }

        if (this.answerData.getMaxAllowed() > optionValues.size()) {
            throw new InvalidQuestionException("The maximum number of required answers allowed can not be greater than the number of options.");
        }

        if (this.answerData.getMinRequired() > this.answerData.getMaxAllowed()) {
            throw new InvalidQuestionException("The minimum number of required answers can not be greater than maximum number of answers allowed.");
        }
    }

    @Override
    @JsonIgnore(false)
    public AnswerData getAnswerData() {
        return super.getAnswerData();
    }

    @Override
    public void checkAnswer(Answer answerModel) {
        super.checkAnswer(answerModel);

        HashSet<String> answerSet = new HashSet<>(Arrays.asList(answerModel.getValues()));
        if (answerSet.size() != answerModel.getValues().length) {
            throw new InvalidAnswerException("Answers can not contain duplicates.");
        }

        checkOptionValues(answerModel);

        int min = this.answerData.getMinRequired();
        if (answerSet.size() < min) {
            throw new InvalidAnswerException("Minimum required number of answers is " + min + ".");
        }

        int max = this.answerData.getMaxAllowed();
        if (answerSet.size() > max) {
            throw new InvalidAnswerException("Maximum number of answers allowed is " + max + ".");
        }
    }

    void checkOptionValues(Answer answerModel) {
        Set<String> optionValues = this.answerData.getOptionValues();
        if (!optionValues.containsAll(Arrays.asList(answerModel.getValues()))) {
            String validAnswersString = String.join(", ", optionValues);
            throw new InvalidAnswerException("Answers must be from the valid set: [" + validAnswersString + "]");
        }
    }
}
