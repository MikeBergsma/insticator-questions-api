package ca.bergs.insticator_questions_api.pojo;

import ca.bergs.insticator_questions_api.model.Answer;
import ca.bergs.insticator_questions_api.model.Question;
import ca.bergs.insticator_questions_api.view.ViewType;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import java.util.Date;
import java.util.UUID;

// TODO: Better organize classes in 'pojo' package

@JsonPropertyOrder({
        "id", "title", "description", "type", "format", "factoryType",
        "tags", "answerData", "createdAt", "updatedAt"
})
public abstract class BaseQuestion {
    @JsonView(ViewType.Public.class)
    private UUID id;
    public UUID getId() { return id; }
    public void setId(UUID id) { this.id = id; }

    @JsonView(ViewType.Public.class)
    private String title;
    public String getTitle() { return title; }
    public void setTitle(String title) { this.title = title; }

    @JsonView(ViewType.Public.class)
    private String description;
    public String getDescription() { return description; }
    public void setDescription(String description) { this.description = description; }

    @JsonView(ViewType.Public.class)
    private QuestionType type;
    public QuestionType getType() { return type; }
    public void setType(QuestionType type) { this.type = type; }

    @JsonView(ViewType.Public.class)
    private QuestionFormat format;
    public QuestionFormat getFormat() { return format; }
    public void setFormat(QuestionFormat format) { this.format = format; }

    @JsonView(ViewType.Public.class)
    private String[] tags;
    public String[] getTags() { return tags; }
    public void setTags(String[] tags) { this.tags = tags; }

    @JsonView(ViewType.Public.class)
    private Date createdAt;
    public Date getCreatedAt() { return createdAt; }
    public void setCreatedAt(Date createdAt) { this.createdAt = createdAt; }

    @JsonView(ViewType.Public.class)
    private Date updatedAt;
    public Date getUpdatedAt() { return updatedAt; }
    public void setUpdatedAt(Date updatedAt) { this.updatedAt = updatedAt; }

    @JsonView(ViewType.Public.class)
    AnswerData answerData;
    public AnswerData getAnswerData() { return answerData; }
    public void setAnswerData(AnswerData answerData) { this.answerData = answerData; }

    @JsonView(ViewType.Public.class)
    public String getFactoryType() {
        return this.getClass().getName().substring(this.getClass().getName().lastIndexOf('.') + 1);
    }

    public void initializeFromModel(Question questionModel) {
        this.id = questionModel.getId();
        this.title = questionModel.getTitle();
        this.description = questionModel.getDescription();
        this.type = questionModel.getType();
        this.format = questionModel.getFormat();
        this.tags = questionModel.getTags();
        this.createdAt = questionModel.getCreatedAt();
        this.updatedAt = questionModel.getUpdatedAt();
        this.answerData = questionModel.getAnswers();
    }

    public void checkAnswer(Answer answerModel) {}
}
