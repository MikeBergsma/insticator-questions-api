package ca.bergs.insticator_questions_api.pojo;

import ca.bergs.insticator_questions_api.view.ViewType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import java.util.ArrayList;
import java.util.List;

@JsonPropertyOrder({ "columns", "rows", "minRequired", "maxAllowed" })
public class MatrixAnswerData extends AnswerData {
    @JsonView(ViewType.Public.class)
    private List<AnswerOption> rows = new ArrayList<>();
    public List<AnswerOption> getRows() { return rows; }
    public void setRows(List<AnswerOption> rows) { this.rows = rows; }

    @JsonView(ViewType.Public.class)
    private List<AnswerOption> columns = new ArrayList<>();
    public List<AnswerOption> getColumns() { return columns; }
    public void setColumns(List<AnswerOption> columns) { this.columns = columns; }

    @Override
    @JsonIgnore
    public AnswerOption[] getOptions() {
        return super.getOptions();
    }

    @Override
    @JsonIgnore
    public AcceptedAnswer getAccepted() {
        return super.getAccepted();
    }
}
