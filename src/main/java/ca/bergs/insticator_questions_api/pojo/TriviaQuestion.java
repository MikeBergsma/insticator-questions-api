package ca.bergs.insticator_questions_api.pojo;

import ca.bergs.insticator_questions_api.exception.InvalidQuestionException;
import ca.bergs.insticator_questions_api.model.Answer;
import ca.bergs.insticator_questions_api.model.Question;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class TriviaQuestion extends ListQuestion {
    @Override
    public void initializeFromModel(Question questionModel) {
        super.initializeFromModel(questionModel);

        HashSet<String> acceptedValues = new HashSet<>(Arrays.asList(this.answerData.getAccepted().getValues()));
        if (acceptedValues.size() != this.answerData.getAccepted().getValues().length) {
            throw new InvalidQuestionException("Accepted answers can not contain duplicates.");
        }

        Set<String> optionValues = this.getAnswerData().getOptionValues();

        if (!optionValues.containsAll(acceptedValues)) {
            String validAnswersString = String.join(", ", optionValues);
            throw new InvalidQuestionException("Accepted answers must be from the valid set: [" + validAnswersString + "]");
        }

        if (this.answerData.getAccepted().getRequiredForAcceptance() == null) {
            this.answerData.getAccepted().setRequiredForAcceptance(1);
        }

        if (this.answerData.getAccepted().getRequiredForAcceptance() > acceptedValues.size()) {
            throw new InvalidQuestionException("The number of required answers for acceptance can not be greater than the number of accepted answers.");
        }

        if (this.answerData.getAccepted().getRequiredForAcceptance() > this.answerData.getMaxAllowed()) {
            throw new InvalidQuestionException("The number of required answers for acceptance can not be greater than maximum number of answers allowed.");
        }
    }

    @Override
    public void checkAnswer(Answer answerModel) {
        super.checkAnswer(answerModel);

        HashSet<String> acceptedValues = new HashSet<>(Arrays.asList(this.answerData.getAccepted().getValues()));
        HashSet<String> answerSet = new HashSet<>(Arrays.asList(answerModel.getValues()));

        answerModel.setIsCorrect(acceptedValues.containsAll(answerSet) &&
                answerSet.size() >= this.answerData.getAccepted().getRequiredForAcceptance());
    }
}
