package ca.bergs.insticator_questions_api.pojo;

import ca.bergs.insticator_questions_api.exception.InvalidAnswerException;
import ca.bergs.insticator_questions_api.exception.InvalidQuestionException;
import ca.bergs.insticator_questions_api.model.Answer;
import ca.bergs.insticator_questions_api.model.Question;

import java.util.*;

public class MatrixQuestion extends ListQuestion {
    private MatrixAnswerData matrixAnswerData = new MatrixAnswerData();

    // TODO: Implement more robust method to store row/column data

    @Override
    public void initializeFromModel(Question questionModel) {
        super.initializeFromModel(questionModel);

        this.matrixAnswerData = new MatrixAnswerData();

        for (AnswerOption option : this.answerData.getOptions()) {
            if (option.getValue().length() > 4 && option.getValue().startsWith("col_")) {
                this.matrixAnswerData.getColumns().add(option);
            } else if (option.getValue().length() > 4 && option.getValue().startsWith("row_")) {
                this.matrixAnswerData.getRows().add(option);
            }
            else {
                throw new InvalidQuestionException("Matrix answer options must start with 'col_' or 'row_'.");
            }
        }

        if (this.matrixAnswerData.getColumns().size() == 0) {
            throw new InvalidQuestionException("Matrix questions require at least 1 column.");
        }

        if (this.matrixAnswerData.getRows().size() == 0) {
            throw new InvalidQuestionException("Matrix questions require at least 1 row.");
        }

        this.matrixAnswerData.setMaxAllowed(this.answerData.getMaxAllowed());
        this.matrixAnswerData.setMinRequired(this.answerData.getMinRequired());
    }

    @Override
    public MatrixAnswerData getAnswerData() {
        return this.matrixAnswerData;
    }

    @Override
    void checkOptionValues(Answer answerModel) {
        Set<String> optionValues = this.answerData.getOptionValues();

        for (String option : answerModel.getValues()) {
            String[] values = option.split(",");

            if (values.length != 2 ||
                    (values[0].startsWith("col_") && values[1].startsWith("col_")) ||
                    (values[0].startsWith("row_") && values[1].startsWith("row_"))
            ) {
                throw new InvalidAnswerException("Answers must be in the format of \"row_value,col_value\".");
            }

            if (!optionValues.containsAll(Arrays.asList(values[0], values[1]))) {
                String validAnswersString = String.join(", ", optionValues);
                throw new InvalidAnswerException("Answers must be from the valid set: [" + validAnswersString + "]");
            }
        }
    }
}
