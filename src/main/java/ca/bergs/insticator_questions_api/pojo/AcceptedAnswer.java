package ca.bergs.insticator_questions_api.pojo;

import ca.bergs.insticator_questions_api.view.ViewType;
import com.fasterxml.jackson.annotation.JsonView;

public class AcceptedAnswer {
    public AcceptedAnswer() {}

    public AcceptedAnswer(String[] values, Integer requiredForAcceptance) {
        this.values = values;
        this.requiredForAcceptance = requiredForAcceptance;
    }

    @JsonView(ViewType.Internal.class)
    private String[] values;
    public String[] getValues() { return values; }
    public void setValues(String[] values) { this.values = values; }

    @JsonView(ViewType.Internal.class)
    private Integer requiredForAcceptance;
    public Integer getRequiredForAcceptance() { return requiredForAcceptance; }
    public void setRequiredForAcceptance(Integer requiredForAcceptance) {
        this.requiredForAcceptance = requiredForAcceptance;
    }
}
