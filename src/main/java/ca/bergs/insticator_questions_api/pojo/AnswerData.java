package ca.bergs.insticator_questions_api.pojo;

import ca.bergs.insticator_questions_api.view.ViewType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "options", "accepted", "minRequired", "maxAllowed" })
public class AnswerData {
    @JsonView(ViewType.Public.class)
    private AnswerOption[] options;
    public AnswerOption[] getOptions() { return options; }
    public void setOptions(AnswerOption[] options) { this.options = options; }

    @JsonView(ViewType.Internal.class)
    private AcceptedAnswer accepted;
    public AcceptedAnswer getAccepted() { return accepted; }
    public void setAccepted(AcceptedAnswer accepted) { this.accepted = accepted; }

    @JsonView(ViewType.Public.class)
    private Integer minRequired;
    public Integer getMinRequired() { return minRequired; }
    public void setMinRequired(Integer minRequired) { this.minRequired = minRequired; }

    @JsonView(ViewType.Public.class)
    private Integer maxAllowed;
    public Integer getMaxAllowed() { return maxAllowed; }
    public void setMaxAllowed(Integer maxAllowed) { this.maxAllowed = maxAllowed; }

    Set<String> getOptionValues() {
        Set<String> optionValues = new HashSet<>();
        for (AnswerOption option : this.options) {
            optionValues.add(option.getValue());
        }
        return optionValues;
    }
}
