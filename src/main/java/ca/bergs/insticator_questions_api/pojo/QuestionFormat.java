package ca.bergs.insticator_questions_api.pojo;

public enum QuestionFormat {
    RADIO,
    CHECKBOX,
    MATRIX,
    // TEXT,
    // NUMBER,
    // DATE
}
