package ca.bergs.insticator_questions_api.pojo;

public enum QuestionType {
    OBJECTIVE,
    TRIVIA
}