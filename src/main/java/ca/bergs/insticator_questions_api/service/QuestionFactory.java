package ca.bergs.insticator_questions_api.service;

import ca.bergs.insticator_questions_api.exception.InvalidQuestionException;
import ca.bergs.insticator_questions_api.model.Question;
import ca.bergs.insticator_questions_api.pojo.*;

public class QuestionFactory {
    public static BaseQuestion createFromModel(Question questionModel) {
        BaseQuestion question = null;

        if (questionModel.getType() == QuestionType.OBJECTIVE) {
            if (questionModel.getFormat() == QuestionFormat.RADIO) {
                question = new PollQuestion();
            } else if (questionModel.getFormat() == QuestionFormat.CHECKBOX) {
                question = new CheckboxQuestion();
            } else if (questionModel.getFormat() == QuestionFormat.MATRIX) {
                question = new MatrixQuestion();
            }
        } else if (questionModel.getType() == QuestionType.TRIVIA) {
            if (questionModel.getFormat() == QuestionFormat.RADIO) {
                question = new TriviaQuestion();
            }
        }

        if (question == null) {
            throw new InvalidQuestionException("Could not create a question from the provided type/format.");
        }

        question.initializeFromModel(questionModel);

        return question;
    }
}
