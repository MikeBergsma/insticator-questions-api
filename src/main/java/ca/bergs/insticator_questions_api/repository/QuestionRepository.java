package ca.bergs.insticator_questions_api.repository;

import ca.bergs.insticator_questions_api.model.Question;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface QuestionRepository extends JpaRepository<Question, UUID> {

    @Query(value = "" +
            "select * " +
            "from insticator_questions.public.questions " +
            "where cast(tags as text[]) @> string_to_array(:tags, ',')", nativeQuery = true)
    Page<Question> findAllByTags(@Param("tags") String tags, Pageable pageable);

    @Query(value= "" +
            "select q.* " +
            "from insticator_questions.public.questions q " +
            "left outer join insticator_questions.public.answers a on q.id = a.question_id and a.user_id  = :userId " +
            "where a.id is null " +
            "order by random() " +
            "limit 1", nativeQuery = true)
    Optional<Question> findOneRandomForUser(@Param("userId") UUID userId);

    @Query(value= "" +
            "select q.* " +
            "from insticator_questions.public.questions q " +
            "left outer join insticator_questions.public.answers a on q.id = a.question_id and a.user_id  = :userId " +
            "where a.id is null and cast(tags as text[]) @> string_to_array(:tags, ',')" +
            "order by random() " +
            "limit 1", nativeQuery = true)
    Optional<Question> findOneRandomForUserByTags(@Param("userId") UUID userId, @Param("tags") String tags);
}
