package ca.bergs.insticator_questions_api.repository;

import ca.bergs.insticator_questions_api.model.Answer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@RepositoryRestResource(collectionResourceRel = "questions", path = "questions")
public interface AnswerRepository extends JpaRepository<Answer, UUID> {
    Page<Answer> findByQuestionId(UUID questionId, Pageable pageable);
    Page<Answer> findByQuestionIdAndUserId(UUID questionId, UUID userId, Pageable pageable);
    Page<Answer> findByUserId(UUID userId, Pageable pageable);

    @Transactional
    void deleteAllByQuestionIdAndUserId(UUID questionId, UUID userID);
    @Transactional
    void deleteAllByQuestionId(UUID questionId);
    @Transactional
    void deleteAllByUserId(UUID userId);
}
