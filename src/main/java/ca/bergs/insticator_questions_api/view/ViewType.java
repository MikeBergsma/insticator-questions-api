package ca.bergs.insticator_questions_api.view;

public class ViewType {
    public static class Public {}
    public static class Internal extends Public {}

    public static class WithoutQuestion extends Public {}
    public static class WithQuestion extends WithoutQuestion {}
}
