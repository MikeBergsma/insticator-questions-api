package ca.bergs.insticator_questions_api.view;

import ca.bergs.insticator_questions_api.model.Answer;
import ca.bergs.insticator_questions_api.pojo.QuestionType;

public class AnswerViewFactory {
    public static AnswerView createViewFromModel(Answer answer) {
        return (answer.getQuestion().getType() == QuestionType.TRIVIA) ?
                new TriviaAnswerView(answer) :
                new AnswerView(answer);
    }
}
