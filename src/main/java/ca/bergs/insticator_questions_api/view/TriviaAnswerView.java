package ca.bergs.insticator_questions_api.view;

import ca.bergs.insticator_questions_api.model.Answer;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

@JsonPropertyOrder({ "id", "userId", "values", "isCorrect", "createdAt", "updatedAt" })
public class TriviaAnswerView extends AnswerView {
    public TriviaAnswerView(Answer answer) {
        super(answer);
    }

    @JsonView(ViewType.WithoutQuestion.class)
    public Boolean getIsCorrect() { return this.answer.getIsCorrect(); }
}
