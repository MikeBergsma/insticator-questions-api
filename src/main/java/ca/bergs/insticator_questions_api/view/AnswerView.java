package ca.bergs.insticator_questions_api.view;

import ca.bergs.insticator_questions_api.model.Answer;
import ca.bergs.insticator_questions_api.pojo.BaseQuestion;
import ca.bergs.insticator_questions_api.service.QuestionFactory;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import java.util.Date;
import java.util.UUID;

@JsonPropertyOrder({ "id", "userId", "values", "createdAt", "updatedAt" })
public class AnswerView {
    Answer answer;

    AnswerView(Answer answer) {
        this.answer = answer;
    }

    @JsonView(ViewType.WithoutQuestion.class)
    public UUID getId() {
        return this.answer.getId();
    }

    @JsonView(ViewType.WithoutQuestion.class)
    public UUID getUserId() {
        return this.answer.getUserId();
    }

    @JsonView(ViewType.WithoutQuestion.class)
    public String[] getValues() { return this.answer.getValues(); }

    @JsonView(ViewType.WithoutQuestion.class)
    public Date getCreatedAt() {
        return this.answer.getCreatedAt();
    }

    @JsonView(ViewType.WithoutQuestion.class)
    public Date getUpdatedAt() {
        return this.answer.getUpdatedAt();
    }

    @JsonView(ViewType.WithQuestion.class)
    public BaseQuestion getQuestion() { return QuestionFactory.createFromModel(this.answer.getQuestion()); }

}
