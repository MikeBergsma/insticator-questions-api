package ca.bergs.insticator_questions_api;

import ca.bergs.insticator_questions_api.model.Answer;
import ca.bergs.insticator_questions_api.model.Question;
import ca.bergs.insticator_questions_api.pojo.*;
import org.springframework.restdocs.headers.RequestHeadersSnippet;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.payload.RequestFieldsSnippet;
import org.springframework.restdocs.request.ParameterDescriptor;

import java.util.*;

import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;

class TestHelper {
    static final List<ParameterDescriptor> PAGE_QUERY_PARAMS = Arrays.asList(
            parameterWithName("page").optional().description("The page number to return"),
            parameterWithName("size").optional().description("The number of results per page"),
            parameterWithName("sort").optional().description("The order by which to sort the results")
    );

    static RequestHeadersSnippet AUTH_HEADER_SNIPPET =
        requestHeaders(headerWithName("Authorization").description("Basic auth credentials"));

    static ParameterDescriptor QUESTION_ID_ROUTE_PARAM = parameterWithName("questionId")
            .description("The id of the question");

    static ParameterDescriptor ANSWER_ID_ROUTE_PARAM = parameterWithName("answerId")
            .description("The id of the answer");

    static ParameterDescriptor USER_ID_QUERY_PARAM = parameterWithName("userId")
            .description("The id of the user");

    static ParameterDescriptor TAGS_QUERY_PARAM =  parameterWithName("tags")
            .optional()
            .description("A comma-delimited list of tags to find questions for");

    static RequestFieldsSnippet ANSWER_FIELDS_SNIPPET = requestFields(
            fieldWithPath("userId").type(JsonFieldType.STRING)
                    .description("The id of user answering the question"),
            fieldWithPath("values").optional().type(JsonFieldType.ARRAY)
                    .description("A list of answer values to the question")
    );

    static RequestFieldsSnippet getQuestionFieldsSnippet(boolean forMatrixFormat) {
        String answerOptionsValueDescription = forMatrixFormat ?
                "The value of row/column (must start with \"row_\" or \"col_\")" :
                "The value the answer option";

        String answerOptionsArrayDescription = forMatrixFormat ?
                "The list of rows/columns in the matrix (must contain at least one of each)" :
                "The available valid answers to the question";

        return requestFields(
                fieldWithPath("title").type(JsonFieldType.STRING)
                        .description("The title of the question"),
                fieldWithPath("description").type(JsonFieldType.STRING)
                        .description("The description of the question"),
                fieldWithPath("format").type(JsonFieldType.STRING)
                        .description("The question format: [RADIO, CHECKBOX, MATRIX]"),
                fieldWithPath("type").type(JsonFieldType.STRING)
                        .description("The question type: [OBJECTIVE, TRIVIA]"),
                fieldWithPath("tags").optional().type(JsonFieldType.ARRAY)
                        .description("A list of tags to associate with the question"),
                fieldWithPath("answers").optional().type(JsonFieldType.OBJECT)
                        .description("The answer data for this question"),
                fieldWithPath("answers.options[]").optional().type(JsonFieldType.ARRAY)
                        .description(answerOptionsArrayDescription),
                fieldWithPath("answers.options[].value").type(JsonFieldType.STRING)
                        .description(answerOptionsValueDescription),
                fieldWithPath("answers.options[].label").type(JsonFieldType.STRING)
                        .description("The label the answer option"),
                fieldWithPath("answers.options.minRequired").optional().type(JsonFieldType.NUMBER)
                        .description("The minimum number of values required to be valid answer"),
                fieldWithPath("answers.options.maxAllowed").optional().type(JsonFieldType.NUMBER)
                        .description("The maximum number of values allowed to be valid answer"),
                fieldWithPath("answers.accepted").optional().type(JsonFieldType.OBJECT)
                        .description("The correct answer data for the question"),
                fieldWithPath("answers.accepted.values").type(JsonFieldType.ARRAY)
                        .description("The list of correct values to the question"),
                fieldWithPath("answers.accepted.requiredForAcceptance").optional().type(JsonFieldType.NUMBER)
                        .description("The number of values required for a correct answer (default: 1)")
        );
    }

    static Question createObjectiveCheckboxQuestion() {
        Question q = new Question();
        q.setId(UUID.randomUUID());
        q.setTitle("About You!");
        q.setDescription("Select all colour which you like:");
        q.setTags(new String[] { "favourite", "colours" });
        q.setCreatedAt(new Date());
        q.setUpdatedAt(new Date());
        AnswerData a = new AnswerData();
        a.setOptions(new AnswerOption[] {
                new AnswerOption("red", "Red" ),
                new AnswerOption("green", "Green" ),
                new AnswerOption("yellow", "Yellow" ),
                new AnswerOption("blue", "Blue" ),
                new AnswerOption("black", "black" ),
                new AnswerOption("purple", "Purple" ),
        });
        a.setMinRequired(0);
        a.setMaxAllowed(6);
        q.setAnswers(a);
        q.setType(QuestionType.OBJECTIVE);
        q.setFormat(QuestionFormat.CHECKBOX);
        return q;
    }

    static Question createObjectiveRadioQuestion(boolean setIdAndAuditDates) {
        Question q = new Question();

        if (setIdAndAuditDates) {
            q.setId(UUID.randomUUID());
            q.setCreatedAt(new Date());
            q.setUpdatedAt(new Date());
        }

        q.setTitle("Hockey!");
        q.setDescription("What is your favourite NHL team?");
        q.setTags(new String[] { "favourite", "sports", "NHL", "hockey" });

        AnswerData a = new AnswerData();
        a.setOptions(new AnswerOption[] {
                new AnswerOption("CGY", "Calgary Flames"),
                new AnswerOption("OTT", "Ottawa Senators"),
                new AnswerOption("WPG", "Winnipeg Jets"),
                new AnswerOption("TOR", "Toronto Maple Leafs"),
                new AnswerOption("VAN", "Vancouver Canucks"),
                new AnswerOption("MTL", "Montreal Canadiens"),
                new AnswerOption("EDM", "Edmonton Oilers"),
        });
        q.setAnswers(a);
        q.setType(QuestionType.OBJECTIVE);
        q.setFormat(QuestionFormat.RADIO);
        return q;
    }

    static Question createTriviaRadioQuestion(boolean setIdAndAuditDates) {
        Question q = new Question();

        if (setIdAndAuditDates) {
            q.setId(UUID.randomUUID());
            q.setCreatedAt(new Date());
            q.setUpdatedAt(new Date());
        }

        q.setTitle("Sports!");
        q.setDescription("Which team won the 2017 NFL Super Bowl?");
        q.setType(QuestionType.TRIVIA);
        q.setFormat(QuestionFormat.RADIO);
        q.setTags(new String[] { "sports", "football", "NFL", "superbowl" });

        AnswerData a = new AnswerData();
        a.setOptions(new AnswerOption[] {
                new AnswerOption("NWE", "New England Patriots"),
                new AnswerOption("ATL", "Atlanta Falcons"),
        });
        a.setAccepted(new AcceptedAnswer( new String[] { "NWE" }, 1));
        q.setAnswers(a);

        return q;
    }

    static Question createObjectiveMatrixQuestion(boolean setIdAndAuditDates) {
        Question q = new Question();

        if (setIdAndAuditDates){
            q.setId(UUID.randomUUID());
            q.setCreatedAt(new Date());
            q.setUpdatedAt(new Date());
        }

        q.setTitle("About You!");
        q.setDescription("What is your gender/age?");
        q.setTags(new String[] { "gender", "age" });

        AnswerData a = new AnswerData();
        a.setOptions(new AnswerOption[] {
                new AnswerOption("col_M", "Male" ),
                new AnswerOption("col_F", "Female" ),
                new AnswerOption("row_under_18", "< 18" ),
                new AnswerOption("row_18_to_35", "18 to 35" ),
                new AnswerOption("row_35_to_55", "35 to 55" ),
                new AnswerOption("row_above_55", "> 55" ),
        });
        q.setAnswers(a);
        q.setType(QuestionType.OBJECTIVE);
        q.setFormat(QuestionFormat.MATRIX);
        return q;
    }

    static Answer createAnswer(Question question, String[] values, Boolean isCorrect) {
        Answer answer = new Answer();
        answer.setId(UUID.randomUUID());
        answer.setQuestion(question);
        answer.setUserId(UUID.randomUUID());
        answer.setValues(values);
        answer.setCreatedAt(new Date());
        answer.setUpdatedAt(new Date());
        answer.setIsCorrect(isCorrect);
        return answer;
    }
}
