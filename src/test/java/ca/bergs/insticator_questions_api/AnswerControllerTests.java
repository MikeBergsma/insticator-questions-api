package ca.bergs.insticator_questions_api;

import ca.bergs.insticator_questions_api.model.Answer;
import ca.bergs.insticator_questions_api.model.Question;
import ca.bergs.insticator_questions_api.repository.AnswerRepository;
import ca.bergs.insticator_questions_api.repository.QuestionRepository;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.*;

import static ca.bergs.insticator_questions_api.TestHelper.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.restdocs.request.RequestDocumentation.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = InsticatorQuestionsApiApplication.class)
public class AnswerControllerTests {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    @MockBean
    QuestionRepository questionRepository;

    @MockBean
    AnswerRepository answerRepository;

    @Rule
    public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/generated-snippets");

    // TODO: Change authentication used in tests.
    private final String adminName = "admin";
    private final String adminPassword = "password";

    private MockMvc mockMvc;
    private RestDocumentationResultHandler documentHandler;

    // TODO: Add missing tests for non-happy paths, validation, etc.

    @Before
    public void setUp(){
        this.documentHandler = document(
                "{method-name}",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())
        );

        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                .addFilter(springSecurityFilterChain)
                .apply(documentationConfiguration(this.restDocumentation))
                .alwaysDo(this.documentHandler)
                .build();
    }

    @Test
    public void postAnswer() throws Exception {
        Question q = createTriviaRadioQuestion(true);

        UUID userId = UUID.randomUUID();

        Answer a = new Answer();
        a.setValues(new String[] { "NWE" });
        a.setUserId(userId);

        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        String newAnswerContent = mapper.writeValueAsString(a);

        a.setId(UUID.randomUUID());
        a.setQuestion(q);
        a.setIsCorrect(true);
        a.setCreatedAt(new Date());
        a.setUpdatedAt(new Date());

        given(answerRepository.save(any(Answer.class))).willReturn(a);
        given(questionRepository.findById(q.getId())).willReturn(Optional.of(q));

        this.mockMvc.perform(post("/questions/{questionId}/answers", q.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(newAnswerContent))
                .andExpect(status().isCreated())
                .andDo(this.documentHandler.document(
                        pathParameters(QUESTION_ID_ROUTE_PARAM),
                        ANSWER_FIELDS_SNIPPET
                ));
    }

    @Test
    public void getAnswer() throws Exception {
        Question q = createTriviaRadioQuestion(true);
        Answer a = createAnswer(q, new String[] { "NWE" }, true);

        given(questionRepository.existsById(q.getId())).willReturn(true);
        given(answerRepository.findById(a.getId())).willReturn(Optional.of(a));

        this.mockMvc.perform(get("/questions/{questionId}/answers/{answerId}", q.getId(), a.getId())
                .with(httpBasic(adminName, adminPassword)))
                .andExpect(status().isOk())
                .andDo(this.documentHandler.document(
                        pathParameters(QUESTION_ID_ROUTE_PARAM).and(ANSWER_ID_ROUTE_PARAM),
                        AUTH_HEADER_SNIPPET
                ));
    }

    @Test
    public void getAnswersForQuestion() throws Exception {
        Question q = createTriviaRadioQuestion(true);
        Pageable pageable = PageRequest.of(0, 20);
        Page<Answer> answersPage = new PageImpl<>(Arrays.asList(
                createAnswer(q, new String[] { "NWE" }, true),
                createAnswer(q, new String[] { "ATL" }, false),
                createAnswer(q, new String[] { "NWE" }, true)
        ), pageable, 3);

        given(questionRepository.existsById(q.getId())).willReturn(true);
        given(answerRepository.findByQuestionId(q.getId(), pageable)).willReturn(answersPage);

        this.mockMvc.perform(get("/questions/{questionId}/answers", q.getId())
                .param("page", "0")
                .param("size", "20")
                .with(httpBasic(adminName, adminPassword)))
                .andExpect(status().isOk())
                .andDo(this.documentHandler.document(
                        pathParameters(QUESTION_ID_ROUTE_PARAM),
                        requestParameters(PAGE_QUERY_PARAMS),
                        AUTH_HEADER_SNIPPET
                ));
    }

    @Test
    public void getAnswersForUser() throws Exception {
        UUID userId = UUID.randomUUID();

        Question q1 = createTriviaRadioQuestion(true);
        Question q2 = createObjectiveCheckboxQuestion();
        Question q3 = createObjectiveRadioQuestion(true);
        Pageable pageable = PageRequest.of(0, 20);
        Page<Answer> answersPage = new PageImpl<>(Arrays.asList(
                createAnswer(q1, new String[] { "NWE" }, true),
                createAnswer(q2, new String[] { "red" }, null),
                createAnswer(q3, new String[] { "TOR" }, null)
        ), pageable, 3);

        given(answerRepository.findByUserId(userId, pageable)).willReturn(answersPage);

        this.mockMvc.perform(get("/answers")
                .param("userId", userId.toString())
                .param("page", "0")
                .param("size", "20")
                .with(httpBasic(adminName, adminPassword)))
                .andExpect(status().isOk())
                .andDo(this.documentHandler.document(
                        requestParameters(USER_ID_QUERY_PARAM).and(PAGE_QUERY_PARAMS),
                        AUTH_HEADER_SNIPPET
                ));
    }

    @Test
    public void putAnswer() throws Exception {
        Question q = createTriviaRadioQuestion(true);
        Answer a = createAnswer(q, new String[] { "ATL" }, false);

        Answer updatedAnswer = new Answer();
        updatedAnswer.setValues(new String[] { "NWE" });
        updatedAnswer.setUserId(a.getUserId());

        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        String updatedAnswerContent = mapper.writeValueAsString(updatedAnswer);

        a.setIsCorrect(true);
        a.setValues(new String[] { "NWE" });
        a.setUpdatedAt(new Date());

        given(questionRepository.findById(q.getId())).willReturn(Optional.of(q));
        given(answerRepository.findById(a.getId())).willReturn(Optional.of(a));
        given(answerRepository.save(any(Answer.class))).willReturn(a);

        this.mockMvc.perform(put("/questions/{questionId}/answers/{answerId}", q.getId(), a.getId())
                .with(httpBasic(adminName, adminPassword))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(updatedAnswerContent))
                .andExpect(status().isOk())
                .andDo(this.documentHandler.document(
                        pathParameters(QUESTION_ID_ROUTE_PARAM).and(ANSWER_ID_ROUTE_PARAM),
                        AUTH_HEADER_SNIPPET,
                        ANSWER_FIELDS_SNIPPET
                ));
    }

    @Test
    public void deleteAnswer() throws Exception {
        Question q = createTriviaRadioQuestion(true);
        Answer a = createAnswer(q, new String[] { "ATL" }, false);

        given(questionRepository.existsById(q.getId())).willReturn(true);
        given(answerRepository.findById(a.getId())).willReturn(Optional.of(a));

        this.mockMvc.perform(delete("/questions/{questionId}/answers/{answerId}", q.getId(), a.getId())
                .with(httpBasic(adminName, adminPassword)))
                .andExpect(status().isNoContent())
                .andDo(this.documentHandler.document(
                        pathParameters(QUESTION_ID_ROUTE_PARAM).and(ANSWER_ID_ROUTE_PARAM),
                        AUTH_HEADER_SNIPPET
                ));
    }

    @Test
    public void deleteAllAnswersForQuestion() throws Exception {
        Question q = createTriviaRadioQuestion(true);

        given(questionRepository.existsById(q.getId())).willReturn(true);

        this.mockMvc.perform(delete("/questions/{questionId}/answers", q.getId())
                .with(httpBasic(adminName, adminPassword)))
                .andExpect(status().isNoContent())
                .andDo(this.documentHandler.document(
                        pathParameters(QUESTION_ID_ROUTE_PARAM),
                        requestParameters(USER_ID_QUERY_PARAM.optional()),
                        AUTH_HEADER_SNIPPET
                ));
    }

    @Test
    public void deleteAllAnswersForUser() throws Exception {
        UUID userId = UUID.randomUUID();
        this.mockMvc.perform(delete("/answers")
                .param("userId", userId.toString())
                .with(httpBasic(adminName, adminPassword)))
                .andExpect(status().isNoContent())
                .andDo(this.documentHandler.document(
                        requestParameters(USER_ID_QUERY_PARAM),
                        AUTH_HEADER_SNIPPET
                ));
    }
}

