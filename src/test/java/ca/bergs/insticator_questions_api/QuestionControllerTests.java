package ca.bergs.insticator_questions_api;

import ca.bergs.insticator_questions_api.model.Question;
import ca.bergs.insticator_questions_api.repository.AnswerRepository;
import ca.bergs.insticator_questions_api.repository.QuestionRepository;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.*;

import static ca.bergs.insticator_questions_api.TestHelper.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = InsticatorQuestionsApiApplication.class)
public class QuestionControllerTests {

	@Autowired
	private WebApplicationContext context;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

	@MockBean
	QuestionRepository questionRepository;

	@MockBean
	AnswerRepository answerRepository;

	@Rule
	public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/generated-snippets");

    // TODO: Change authentication used in tests.
	private final String adminName = "admin";
    private final String adminPassword = "password";

    private MockMvc mockMvc;
    private RestDocumentationResultHandler documentHandler;

	// TODO: Add missing tests for non-happy paths, validation, etc.

    @Before
	public void setUp(){
        this.documentHandler = document(
                "{method-name}",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())
        );

	    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                .addFilter(springSecurityFilterChain)
				.apply(documentationConfiguration(this.restDocumentation))
                .alwaysDo(this.documentHandler)
				.build();
	}

    @Test
    public void postQuestion() throws Exception {
        Question q = createTriviaRadioQuestion(false);

        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        String newQuestionContent = mapper.writeValueAsString(q);

        q.setId(UUID.randomUUID());
        q.setCreatedAt(new Date());
        q.setUpdatedAt(new Date());

        given(questionRepository.save(any(Question.class))).willReturn(q);

        this.mockMvc.perform(post("/questions")
                .with(httpBasic(adminName, adminPassword))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(newQuestionContent))
                .andExpect(status().isCreated())
                .andDo(this.documentHandler.document(
                        AUTH_HEADER_SNIPPET,
                        getQuestionFieldsSnippet(false)
                ));
    }

    @Test
    public void postMatrixQuestion() throws Exception {
        Question q = createObjectiveMatrixQuestion(false);

        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        String newQuestionContent = mapper.writeValueAsString(q);

        q.setId(UUID.randomUUID());
        q.setCreatedAt(new Date());
        q.setUpdatedAt(new Date());

        given(questionRepository.save(any(Question.class))).willReturn(q);

        this.mockMvc.perform(post("/questions")
                .with(httpBasic(adminName, adminPassword))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(newQuestionContent))
                .andExpect(status().isCreated())
                .andDo(this.documentHandler.document(
                        AUTH_HEADER_SNIPPET,
                        getQuestionFieldsSnippet(true)
                ));
    }

    @Test
    public void getQuestion() throws Exception {
        Question q = createObjectiveRadioQuestion(true);
        given(questionRepository.findById(q.getId())).willReturn(Optional.of(q));

        this.mockMvc.perform(get("/questions/{questionId}", q.getId()))
                .andExpect(status().isOk())
                .andDo(this.documentHandler.document(pathParameters(QUESTION_ID_ROUTE_PARAM)));
    }

    @Test
    public void getQuestions() throws Exception {
        Pageable pageable = PageRequest.of(0, 20);

        Page<Question> questionsPage = new PageImpl<>(
                Arrays.asList(
                        createObjectiveCheckboxQuestion(),
                        createObjectiveRadioQuestion(true),
                        createTriviaRadioQuestion(true)
                ), pageable, 3);

        given(questionRepository.findAll(pageable)).willReturn(questionsPage);

        this.mockMvc.perform(get("/questions")
                .param("page", "0")
                .param("size", "20")
                .with(httpBasic(adminName, adminPassword)))
                .andExpect(status().isOk())
                .andDo(this.documentHandler.document(
                        AUTH_HEADER_SNIPPET,
                        requestParameters(PAGE_QUERY_PARAMS).and(TAGS_QUERY_PARAM)
                ));
    }

    @Test
    public void getRandomQuestionForUser() throws Exception {
        UUID userId = UUID.randomUUID();
        given(questionRepository.findOneRandomForUser(userId))
                .willReturn(Optional.of(createTriviaRadioQuestion(true)));

        this.mockMvc.perform(get("/questions/random?userId={userId}", userId.toString()))
                .andExpect(status().isOk())
                .andDo(this.documentHandler.document(
                        requestParameters(USER_ID_QUERY_PARAM).and(TAGS_QUERY_PARAM))
                );
    }

    @Test
    public void putQuestion() throws Exception {
        Question q = createTriviaRadioQuestion(false);
        q.setTitle("NFL Trivia!");

        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        String updatedQuestionContent = mapper.writeValueAsString(q);

        q.setId(UUID.randomUUID());
        q.setCreatedAt(new Date());
        q.setUpdatedAt(new Date());

        given(questionRepository.findById(q.getId())).willReturn(Optional.of(q));
        given(questionRepository.save(any(Question.class))).willReturn(q);
        given(answerRepository.findByQuestionId(q.getId(), Pageable.unpaged())).willReturn(Page.empty());

        this.mockMvc.perform(put("/questions/{questionId}", q.getId())
                .with(httpBasic(adminName, adminPassword))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(updatedQuestionContent))
                .andExpect(status().isOk())
                .andDo(this.documentHandler.document(
                        pathParameters(QUESTION_ID_ROUTE_PARAM),
                        AUTH_HEADER_SNIPPET,
                        getQuestionFieldsSnippet(false)
                ));
    }

    @Test
    public void deleteQuestion() throws Exception {
        Question q = createObjectiveRadioQuestion(true);

        given(questionRepository.findById(q.getId())).willReturn(Optional.of(q));

        this.mockMvc.perform(delete("/questions/{questionId}", q.getId())
                .with(httpBasic(adminName, adminPassword)))
                .andExpect(status().isNoContent())
                .andDo(this.documentHandler.document(
                        pathParameters(QUESTION_ID_ROUTE_PARAM),
                        AUTH_HEADER_SNIPPET
                ));
    }
}

